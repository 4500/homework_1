# Caleb Freeman
# 8/21/2019
# CS 4500
# ###explanation of what the program is designed to do###
# This program is designed to play a connected diagraph game:
#   1) Place a magnetic marker in circle #1, and put a check mark in circle #1.
#   The circle where the marker resides is called the “current circle.”
#   2) Randomly choose among the out arrows in the current circle.
#   (If there is only one out arrow that choice is trivial) In this selection, all the out arrows should
#   be equally likely to be picked.
#   3) Move the marker to the circle pointed to by the out arrow. This becomes the new current circle.
#   4) Put a check mark in the current circle.
#   5) If all the circles have at least one check mark, stop the game. If not, go to step 2 and repeat.
# The central data structures used are pythons lists implementation
# External input file HW1infile.txt that consists of:
#   The first line has only the number N, the number of circles that will be used in your game.
#   The second line has the number K, the number of arrows you will “drawing” between the circles.
#   The next K lines designate the arrows, one arrow per line.
#   Each arrow line consists of two numbers, each number being
#   one of circles in the game. These two numbers are separated by a single blank.
#   The first number designates the circle
#   that is the source (back end) of the arrow; the second number designates the circle that is the destination
#   (pointed end) of the arrow.
# TODO need to review the logic for the max number of checks in any one circle

import random  # for selecting randomly which outArrow to follow

# read input file named HW1infile.txt
with open('HW1infile.txt') as f:
    dataArray = []
    lines = f.read().split()
    try:
        for line in lines:
            dataArray.append(int(line))
    except:
        print("sorry input file must contain only integers")
        # writing error output to output file
        with open('HW1freemanOutfile.txt', 'w') as the_file:
            the_file.write('Exception: Input file must only contain integers\n')
        exit(1)

# line one the number of circles must be a single integer N between 2 and 10
try:
    if isinstance(dataArray[0], int) and (2 <= dataArray[0] <= 10):
        print("")
    else:
        raise ValueError
except ValueError:
    print("Sorry the infile first line must be an integer between 2 and 10 ")
    with open('HW1freemanOutfile.txt', 'w') as the_file:
        the_file.write('Exception: FIrst line of input file must contain a single integer between 2 and 10\n')
    exit(1)

# line two is a single integer K that is equal to the number of arrows
# (length of dataArray - 2 / 2 ) or the length of the array minus the two K and N inputs
# divided by two, for the input and output for each arrow, will equal K
try:
    # comparing k, the number of arrows is
    k = dataArray[1]
    if ((len(dataArray) - 2) / 2) == dataArray[1]:
        print("")
    else:
        raise ValueError
except ValueError:
    print("Sorry invalid arrow count")
    with open('HW1freemanOutfile.txt', 'w') as the_file:
        the_file.write('Exception: Second line of input must contain a single integer K equal to the number arrows\n')
    exit(1)

# playing the GAME
# POPULATING list to check if the circles have been visited, and to hold the arrow coordinates between circles
n = dataArray[0];
outArrowsStartingIndex = []
outArrowEndingIndex = []
gameCheck = []
# populate an list with of size N, will remove a value each time
for i in range(n):
    gameCheck.append(1)

# feeding the first coordinate of arrows pairs into an array will have matching indices with outArrowEndingIndex
for i in range(2, len(dataArray), 2):
    outArrowsStartingIndex.append(dataArray[i])

# feeding the second coordinate of arrows pairs into an array will have matching indices with outArrowStatingIndex
for i in range(3, len(dataArray), 2):
    outArrowEndingIndex.append(dataArray[i])

# game starts with the first circle being filled
# placeholder for position in the game, we start at the first position
# odd numbers in dataArray[3] ... dataArray[i] are circle starting positions.
listOfOutArrows = []
current = 0
gameCheck[current] = 0
current = 1
numberOfChecks = 0
maxChecksPerCircle = 0
maxChecksPerCircleHold = 0

# while loop to play the game
while sum(gameCheck) != 0:
    outArrowSearch = current  # index of the current circle
    # if loop that searches for which circle required the most checks in any one circle
    if maxChecksPerCircle > maxChecksPerCircleHold:
        maxChecksPerCircleHold = maxChecksPerCircle
        maxChecksPerCircle = 0
    # for loop that searches for index values of arrows that are connected
    for i in range(len(outArrowsStartingIndex)):
        if outArrowsStartingIndex[i] == outArrowSearch:  # appending a list of arrow index leaving the current circle
            listOfOutArrows.append(i)
    randomArrowChoice = random.choice(listOfOutArrows)  # rand select an index for an output arrow from current circle
    current = outArrowEndingIndex[randomArrowChoice]  # current index is always one less ths line needs testing!!!
    listOfOutArrows.clear()  # clears the list of possible out arrow choices after selecting one randomly
    if randomArrowChoice <= len(gameCheck):  # bounds checking before marking a circle as visited
        solIndex = outArrowEndingIndex[randomArrowChoice]
        if gameCheck[solIndex - 1] != 0:  # list of sol. indices are off by one because circle count does not start at0
            gameCheck[solIndex - 1] = 0
            numberOfChecks += 1
            maxChecksPerCircle += 1
        else:
            numberOfChecks += 1
            maxChecksPerCircle += 1
    else:
        continue

# finding the average number o checks
averageChecks = (numberOfChecks + 1) / n  # plus one for the first check

# output criteria
print("The number of circles that were used for this game: ", n)
print("The number of arrows that were used for this game: ", k)
print("The total number of checks on all the circles combined: ", (numberOfChecks + 1))  # plus one for the first check
print("The average number of checks in a circle marked during the game: ", averageChecks)
print("The maximum number of checks in any one circle: ", maxChecksPerCircleHold)
# opening output file to write required criteria too
with open('HW1freemanOutfile.txt', 'w') as the_file:
    the_file.write("The number of circles that were used for this game: " + str(n) + "\n")
    the_file.write("The number of arrows that were used for this game: " + str(k) + "\n")
    the_file.write("The total number of checks on all the circles combined: " + str((numberOfChecks + 1)) + "\n")
    the_file.write("The average number of checks in a circle marked during the game: " + str(averageChecks) + "\n")
    the_file.write("The maximum number of checks in any one circle: " + str(maxChecksPerCircleHold) + "\n")
exit(0)
